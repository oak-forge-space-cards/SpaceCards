﻿using Microsoft.AspNetCore.Mvc;
using SpaceCardGame.Domain.Models;
using SpaceCardGame.Infrastructur.Services.GamePropertiesService;

namespace GameProperties.Controllers
{
    [ApiController]
    [Route("apigameproperties")]
    public class GamePropertiesController : ControllerBase

    {
        private IGamePropertiesModelService _gamePropertiesModelService { get; }

        public GamePropertiesController(IGamePropertiesModelService gamePropertiesModelService)
        {
            _gamePropertiesModelService = gamePropertiesModelService;
        }
        //get all
        [HttpGet("GetGameProperties")]
        public IEnumerable<GamePropertiesModel> GetAll(GamePropertiesModel gamePropertiesModel)
        {
            return _gamePropertiesModelService.GetAll(gamePropertiesModel);
        }
        //get
        [HttpGet("GetStageId")]
        public IEnumerable<GamePropertiesModel> GetStageId(int stageId)
        {
            if (stageId == 0)
            {
                return (IEnumerable<GamePropertiesModel>)NotFound();
            }
            return _gamePropertiesModelService.GetStageId(stageId);

        }
        [HttpGet("GetNumberOfCards")]
        public IEnumerable<GamePropertiesModel> GetNumberOfCards(int numberOfCards)
        {
            return _gamePropertiesModelService.GetNumberOfCards(numberOfCards);

        }
        [HttpGet("GetNumberOfPlayers")]
        public IEnumerable<GamePropertiesModel> GetNumberOfPlayers(int numberOfPlayers)
        {
            return _gamePropertiesModelService.GetNumberOfPlayers(numberOfPlayers);

        }
        [HttpGet("GetGameTime")]
        public IEnumerable<GamePropertiesModel> GetGameTime(int gameTime)
        {
            return _gamePropertiesModelService.GetGameTime(gameTime);

        }
        // Delete
        [HttpDelete("DeleteGameProperties")]
        public void DeleteGameProperties(GamePropertiesModel gamePropertiesModel)
        {
            _gamePropertiesModelService.DeleteGameProperties(gamePropertiesModel);

        }
        // Create
        [HttpPost("CreateGameProperties")]
        public GamePropertiesModel CreateGamePropertiesModel(GamePropertiesModel gamePropertiesModel)
        {
            return _gamePropertiesModelService.CreateGamePropertiesModel(gamePropertiesModel);
        }
        //save
        [HttpPut("SaveGameProperties")]
        public void Save()
        {
            _gamePropertiesModelService.Save();
            // return ok?
        }
        //edit
        [HttpPatch("EditGameProperties")]
        public GamePropertiesModel EditGamePropertiesModel(GamePropertiesModel gamePropertiesModelToEdit)
        {
            return _gamePropertiesModelService.EditGamePropertiesModel(gamePropertiesModelToEdit);
        }

    }
}