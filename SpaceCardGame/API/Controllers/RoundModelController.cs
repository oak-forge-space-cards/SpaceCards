﻿using Microsoft.AspNetCore.Mvc;
using SpaceCardGame.Domain.Models;
using SpaceCardGame.Infrastructur.Services.RoundModelService;

namespace RoundModel1.Controllers
{
    [ApiController]
    [Route("apiroundmodel")]
    public class RoundModelController : ControllerBase
    {

        private IRoundModelService _roundModelService { get; }
        public RoundModelController(IRoundModelService roundModelService)
        {
            _roundModelService = roundModelService;
        }

        //get all
        [HttpGet("GetRoundModel")]
        public IEnumerable<RoundModel> GetAll(RoundModel roundModel)
        {
            return _roundModelService.GetAll(roundModel);
        }

        [HttpGet("GetRoundId")]
        public IEnumerable<RoundModel> GetRoundId(int roundId)
        {
            if (roundId == 0)
            {
                return (IQueryable<RoundModel>)NotFound();
            }
            return _roundModelService.GetRoundId(roundId);
        }

        [HttpGet("OperationType")]

        public IEnumerable<RoundModel> OperationType(OperationType operationType)
        {
            return _roundModelService.GetOperationType(operationType);
        }


        // Delete
        [HttpDelete("DeleteRoundModel")]
        public void DeleteRoundModel(RoundModel roundModel)
        {
            _roundModelService.DeleteRoundModel(roundModel);

        }
        // Create
        [HttpPost("CreateRoundModel")]
        public RoundModel CreateRoundModel(RoundModel roundModel)
        {
            return _roundModelService.CreateRoundModel(roundModel);
        }
        //save
        [HttpPut("SaveRoundModel")]
        public void Save()
        {
            _roundModelService.Save();
        }
        //edit
        [HttpPatch("EditRoundModel")]
        public RoundModel EditRoundModel(RoundModel roundModelToEdit)
        {
            return _roundModelService.EditRoundModel(roundModelToEdit);
        }

    }
}
