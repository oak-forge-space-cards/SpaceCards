using Microsoft.EntityFrameworkCore;
using SpaceCardGame.Infrastructur;
using SpaceCardGame.Infrastructur.Services.ExampleService;
using SpaceCardGame.Infrastructur.Services.ExampleService.Interface;
using SpaceCardGame.Infrastructur.Services.GamePropertiesService;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Komentarz
builder.Services.AddDbContext<DbGameContext>(builder =>
{
    builder.UseSqlServer(@"Data Source=DESKTOP-8FBCEN1\SQLEXPRESS;Initial Catalog=DbGame;Integrated Security=True; TrustServerCertificate = True");
});


//builder.Services.AddScoped<IGamePropertiesModelService, GamePropertiesModelService>();

var app = builder.Build();

//Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseDeveloperExceptionPage();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
