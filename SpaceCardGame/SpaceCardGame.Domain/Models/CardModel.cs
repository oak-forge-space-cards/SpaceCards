﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata;
using System.ComponentModel.DataAnnotations;

namespace SpaceCardGame.Domain.Models
{
    public class CardModel
    {
        public CardModel() { }

        [Key]
        public int CardId { get; set; }
        public string Name { get; set; }
        public int Strenght { get; set; }
        public int Power { get; set; }
        public int DefenseFieldValue { get; set; }
        public int DefenseArmor { get; set; }

        public CardModel(int cardId, string name, int strenght, int power, int defenseFieldValue, int defenseArmor)
        {
            this.CardId = cardId;
            this.Name = name;
            this.Strenght = strenght;
            this.Power = power;
            this.DefenseFieldValue = defenseFieldValue;
            this.DefenseArmor = defenseArmor;
        }


    }
}
