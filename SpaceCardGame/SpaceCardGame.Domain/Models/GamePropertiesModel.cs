using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata;
using System.ComponentModel.DataAnnotations;

namespace SpaceCardGame.Domain.Models
{
    public class GamePropertiesModel
    {
        public GamePropertiesModel() { }
        [Key]
        public int StageId { get; set; }


        public int NumberOfCards { get; set; }
        public int NumberOfPlayers { get; set; }
        public int GameTime { get; set; }


        public GamePropertiesModel(int numberOfCards, int numberOfPlayers, int gameTime, int stageId)
        {
            this.StageId = stageId;
            this.NumberOfCards = numberOfCards;
            this.NumberOfPlayers = numberOfPlayers;
            this.GameTime = gameTime;

        }
    }
}
