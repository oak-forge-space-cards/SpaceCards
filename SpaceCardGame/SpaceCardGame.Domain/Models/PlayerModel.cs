using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata;
using System.ComponentModel.DataAnnotations;

namespace SpaceCardGame.Domain.Models
{
    public class PlayerModel
    {
        public PlayerModel() { }

        [Key]
        public int PlayerId { get; set; }

        public string Name { get; }
        public int Deck { get; }
        public int MatchesCount { get; }
        public int WinCount { get; }
        public int LoseCount { get; }

        public PlayerModel(int playerId, string name, int deck, int matchesCount, int winCount, int loseCount)
        {
            this.PlayerId = playerId;
            this.Name = name;
            this.Deck = deck;
            this.MatchesCount = matchesCount;
            this.WinCount = winCount;
            this.LoseCount = loseCount;
        }

    }
}