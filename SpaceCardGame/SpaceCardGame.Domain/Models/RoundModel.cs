﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceCardGame.Domain.Models
{
    public enum OperationType
    {
        Battle,
        CaptureTheFlag,
        Elimination,
        AllForOne
    }

    public class RoundModel
    {
        public int RoundId { get; }
        
        public OperationType operationType;

        

        public RoundModel(int roundId, OperationType operationType)
        {
            this.RoundId = roundId;
           
            this.operationType = operationType;
        }
    }
}


