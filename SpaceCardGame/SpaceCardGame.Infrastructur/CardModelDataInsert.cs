﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceCardGame.Infrastructur.DataInitialize
{
    public class CardModelDataInsert
    {

        public void CardModelDataInitialize()
        {
            string connectionString = @"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=DbGame;Integrated Security=True";
            SqlConnection connection = new SqlConnection(@connectionString);
            string query =
                "TRUNCATE TABLE dbo.Card;" +
                "INSERT INTO dbo.Card (Name, Strenght, Power, DefenseFieldValue, DefenseArmor) VALUES(1,1,1,1,1)";

            SqlCommand command = new SqlCommand(query, connection);
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                Console.WriteLine("Records Inserted Successfully");
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error Generated. Details: " + e.ToString());
            }
            finally
            {
                connection.Close();
            }
        }
    }
}