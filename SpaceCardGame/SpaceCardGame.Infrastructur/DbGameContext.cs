﻿using Microsoft.EntityFrameworkCore;
using SpaceCardGame.Domain.Models;


namespace SpaceCardGame.Infrastructur;

public class DbGameContext : DbContext
{
    public DbGameContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<CardModel> Card => Set<CardModel>();
    public DbSet<GamePropertiesModel> GamePropertiesModel => Set<GamePropertiesModel>();
    public DbSet<PlayerModel> Player => Set<PlayerModel>();
    public DbSet<RoundModel> Round => Set<RoundModel>();

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        //modelBuilder.Entity<CardModel>(
        //     c =>
        //     {
        //         c.HasKey(e => e.CardId);
        //         c.Property(e => e.Strenght);
        //         c.Property(e => e.Power);
        //         c.Property(e => e.DefenseFieldValue);
        //         c.Property(e => e.Name);
        //     });

        //modelBuilder.Entity<GamePropertiesModel>(
        //     a =>
        //     {
        //         a.HasKey(x => x.StageId);
        //         a.Property(x => x.NumberOfCards);
        //         a.Property(x => x.NumberOfPlayers);
        //         a.Property(x => x.GameTime);
        //     });

        //modelBuilder.Entity<PlayerModel>(
        //     b =>
        //     {
        //         b.HasKey(y => y.PlayerId);
        //         b.Property(y => y.Name);
        //         b.Property(y => y.Deck);
        //         b.Property(y => y.MatchesCount);
        //         b.Property(y => y.WinCount);
        //         b.Property(y => y.LoseCount);
        //     });


        //modelBuilder.Entity<RoundModel>(
        //     d =>
        //     {
        //         d.HasKey(z => z.RoundId);
        //         d.Property(z => z.PlayerIdList);
        //         d.Property(z => z.CardIdList);
        //         d.Property(z => z.operationType);

        //     });
    }
    //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //{
    //    optionsBuilder.UseSqlServer("Server=ESSA\\SQLEXPRESS;Database=staz;Trusted_Connection=True");

    //}
}
