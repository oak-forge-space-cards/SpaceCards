﻿using SpaceCardGame.Domain.Models;

namespace SpaceCardGame.Infrastructur.GamePropertiesModelCRUD
{
    public class EntityGamePropertiesModelRepository : IGamePropertiesModelRepository
    {
        private readonly DbGameContext _entities;

        public EntityGamePropertiesModelRepository(DbGameContext context)
        {
            _entities = context;
        }

        public GamePropertiesModel GetGamePropertiesModel(int id)
        {
            var gamePropertiesModel = _entities.GamePropertiesModel.Find(id);
            return gamePropertiesModel;
        }

        public IEnumerable<GamePropertiesModel> ListGamePropertiesModel()
        {
            return _entities.GamePropertiesModel.ToList();
        }

        public GamePropertiesModel CreateGamePropertiesModel(GamePropertiesModel GamePropertiesModelToCreate)
        {
            _entities.GamePropertiesModel.Add(GamePropertiesModelToCreate);
            _entities.SaveChanges();
            return GamePropertiesModelToCreate;
        }

        public GamePropertiesModel EditGamePropertiesModel(GamePropertiesModel gamePropertiesModelToEdit)
        {
            var originalGamePropertiesModel = GetGamePropertiesModel(gamePropertiesModelToEdit.StageId);
            _entities.GamePropertiesModel.Update(gamePropertiesModelToEdit);
            //_entities.ApplyPropertyChanges(originalPlayerModel.PlayerId.EntitySetName, playerModelToEdit);
            _entities.SaveChanges();
            return gamePropertiesModelToEdit;
        }

        public void DeleteGamePropertiesModel(GamePropertiesModel gamePropertiesModelToDelete)
        {
            var originalGamePropertiesModel = GetGamePropertiesModel(gamePropertiesModelToDelete.StageId);
            _entities.GamePropertiesModel.Remove(originalGamePropertiesModel);
            _entities.SaveChanges();
        }

        public void Save()
        {
            _entities.SaveChanges();
        }
    }
}
