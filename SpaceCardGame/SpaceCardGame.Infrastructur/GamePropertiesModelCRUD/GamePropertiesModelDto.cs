﻿namespace SpaceCardGame.Infrastructur.GamePropertiesModelCRUD
{
    internal class GamePropertiesModelDto
    {
        public GamePropertiesModelDto() { }
        public int StageId { get; }
        public int NumberOfCards { get; }
        public int NumberOfPlayers { get; }
        public int GameTime { get; }
        public GamePropertiesModelDto(int numberOfCards, int numberOfPlayers, int gameTime, int stageId)
        {
            this.StageId = stageId;
            this.NumberOfCards = numberOfCards;
            this.NumberOfPlayers = numberOfPlayers;
            this.GameTime = gameTime;
        }
    }
}
