﻿using SpaceCardGame.Domain.Models;

namespace SpaceCardGame.Infrastructur.GamePropertiesModelCRUD
{
    public interface IGamePropertiesModelRepository
    {
        GamePropertiesModel CreateGamePropertiesModel(GamePropertiesModel gamePropertiesModelToCreate);
        void DeleteGamePropertiesModel(GamePropertiesModel gamePropertiesModelToDelete);
        GamePropertiesModel EditGamePropertiesModel(GamePropertiesModel gamePropertiesModelToUpdate);
        GamePropertiesModel GetGamePropertiesModel(int id);
        IEnumerable<GamePropertiesModel> ListGamePropertiesModel();
        void Save();

    }
}