﻿using SpaceCardGame.Domain.Models;

namespace SpaceCardGame.Infrastructur.PlayerModelCRUD
{
    public class EntityPlayerModelRepository : IPlayerModelRepository
    {
        private readonly DbGameContext _entities;

        public EntityPlayerModelRepository(DbGameContext context)
        {
            _entities = context;
        }

        public PlayerModel GetPlayerModel(int id)
        {
            var playerModel = _entities.Player.Find(id);
            return playerModel;

            //return _entities.Player.Find(id);

            //return (from b in _entities.Player
            //        where b.PlayerId == id
            //        select b).FirstOrDefault();
        }

        public IQueryable<PlayerModel> ListPlayerModel()
        {
            return _entities.Player;
        }

        public PlayerModel CreatePlayerModel(PlayerModel PlayerModelToCreate)
        {
            _entities.Player.Add(PlayerModelToCreate);
            _entities.SaveChanges();
            return PlayerModelToCreate;
        }

        public PlayerModel EditPlayerModel(PlayerModel playerModelToEdit)
        {
            var originalPlayerModel = GetPlayerModel(playerModelToEdit.PlayerId);
            _entities.Player.Update(playerModelToEdit);
            //_entities.ApplyPropertyChanges(originalPlayerModel.PlayerId.EntitySetName, playerModelToEdit);
            _entities.SaveChanges();
            return playerModelToEdit;
        }

        public void DeletePlayerModel(PlayerModel playerModelToDelete)
        {
            var originalPlayerModel = GetPlayerModel(playerModelToDelete.PlayerId);
            _entities.Player.Remove(originalPlayerModel);
            _entities.SaveChanges();
        }
    }
}
