﻿using SpaceCardGame.Domain.Models;

namespace SpaceCardGame.Infrastructur.PlayerModelCRUD
{
    public interface IPlayerModelRepository
    {
        PlayerModel CreatePlayerModel(PlayerModel playerModelToCreate);
        void DeletePlayerModel(PlayerModel playerModelToDelete);
        PlayerModel EditPlayerModel(PlayerModel playerModelToUpdate);
        PlayerModel GetPlayerModel(int id);
        IQueryable<PlayerModel> ListPlayerModel();
    }
}
