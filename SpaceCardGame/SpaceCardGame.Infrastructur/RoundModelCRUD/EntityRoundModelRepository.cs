using SpaceCardGame.Domain.Models;

namespace SpaceCardGame.Infrastructur.RoundModelCRUD
{
    public class EntityRoundModelRepository : IRoundModelRepository
    {
        private readonly DbGameContext _entities;

        public EntityRoundModelRepository(DbGameContext context)
        {
            _entities = context;
        }

        public IEnumerable<RoundModel> GetAll()
        {
            return _entities.Round.ToList();
        }

        public RoundModel GetRoundModel(int id)
        {
            var roundModel = _entities.Round.Find(id);
            return roundModel;
        }

        public IEnumerable<RoundModel> ListRoundModel()
        {
            return _entities.Round.ToList();
        }

        public RoundModel CreateRoundModel(RoundModel roundModelToCreate)
        {
            _entities.Round.Add(roundModelToCreate);
            _entities.SaveChanges();
            return roundModelToCreate;
        }

        public RoundModel EditRoundModel(RoundModel roundModelToEdit)
        {
            var originalRoundModel = GetRoundModel(roundModelToEdit.RoundId);
            _entities.Round.Update(roundModelToEdit);
            _entities.SaveChanges();
            return roundModelToEdit;
        }

        public void DeleteRoundModel(RoundModel roundModelToDelete)
        {
            var originalRoundModel = GetRoundModel(roundModelToDelete.RoundId);
            _entities.Round.Remove(originalRoundModel);
            _entities.SaveChanges();
        }

        public void Save()
        {
            _entities.SaveChanges();
        }
    }
}

