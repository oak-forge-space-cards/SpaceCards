using SpaceCardGame.Domain.Models;
using System;
using System.Collections.Generic;

namespace SpaceCardGame.Infrastructur.RoundModelCRUD
{
    public interface IRoundModelRepository
    {
        IEnumerable<RoundModel> GetAll();
        RoundModel GetRoundModel(int roundId);
        RoundModel CreateRoundModel(RoundModel roundModelToCreate);
        void DeleteRoundModel(RoundModel roundModelToDelete);
        RoundModel EditRoundModel(RoundModel roundModelToUpdate);
        IEnumerable<RoundModel> ListRoundModel();

        void Save();
    }

}
