﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceCardGame.Infrastructur.RoundModelCRUD
{


    internal class RoundModelDto
    {
        public enum OperationType
        {
            Battle,
            CaptureTheFlag,
            Elimination,
            AllForOne
        }

        public int RoundId { get; }
        public OperationType operationType;



        public RoundModelDto(int roundId, OperationType operationType)
        {
            this.RoundId = roundId;
            this.operationType = operationType;
        }
    }
}