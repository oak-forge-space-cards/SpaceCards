﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceCardGame.Domain.Models;



namespace SpaceCardGame.Infrastructur.RoundModelCRUD
{
    public class RoundModelSeed
    {
        private readonly DbGameContext dbGameContext;

        public RoundModelSeed(DbGameContext dbGameContext)
        {
            this.dbGameContext = dbGameContext;
        }
        public void Seed(RoundModel roundModel)
        {
            if (!dbGameContext.Round.Any())
            {
                var roundmodel = new List<RoundModel>();
            }

            new RoundModel(1, OperationType.Battle);
            new RoundModel(22, OperationType.CaptureTheFlag);

            {

            };
            dbGameContext.Round.AddRange(roundModel);
            dbGameContext.SaveChanges();
        }
    }
}