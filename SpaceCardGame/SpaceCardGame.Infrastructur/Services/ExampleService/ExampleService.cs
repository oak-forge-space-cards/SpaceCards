﻿using SpaceCardGame.Domain.Models;
using SpaceCardGame.Infrastructur.PlayerModelCRUD;
using SpaceCardGame.Infrastructur.Services.ExampleService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceCardGame.Infrastructur.Services.ExampleService
{
    public class ExampleService : IExampleService
    {
        IPlayerModelRepository _playerModelRepository { get; }
        public ExampleService(IPlayerModelRepository playerModelRepository)
        {
            _playerModelRepository = playerModelRepository;
        }

        public virtual IEnumerable<PlayerModel> GetPlayerModelThatLoosXTimes(int loseNumber)
        {
            return _playerModelRepository.ListPlayerModel().Where(p=>p.LoseCount == loseNumber).ToList();
        }
    }
}
