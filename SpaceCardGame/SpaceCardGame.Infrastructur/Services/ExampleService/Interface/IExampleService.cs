﻿using SpaceCardGame.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceCardGame.Infrastructur.Services.ExampleService.Interface
{
    public interface IExampleService
    {
        IEnumerable<PlayerModel> GetPlayerModelThatLoosXTimes(int loseNumber);
    }
}
