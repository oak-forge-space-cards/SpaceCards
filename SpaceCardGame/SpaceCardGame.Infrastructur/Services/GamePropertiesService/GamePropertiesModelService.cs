﻿using SpaceCardGame.Domain.Models;
using SpaceCardGame.Infrastructur.GamePropertiesModelCRUD;

namespace SpaceCardGame.Infrastructur.Services.GamePropertiesService
{
    public class GamePropertiesModelService : IGamePropertiesModelService
    {
        public IGamePropertiesModelRepository? _gamePropertiesModelRepository { get; }

        public GamePropertiesModelService(IGamePropertiesModelRepository gamePropertiesModelRepository)
        {
            _gamePropertiesModelRepository = gamePropertiesModelRepository;
        }
        //get
        public virtual IEnumerable<GamePropertiesModel> GetGameTime(int gameTime)
        {
            return _gamePropertiesModelRepository.ListGamePropertiesModel().Where(g => g.GameTime == gameTime).ToList();
        }

        public virtual IEnumerable<GamePropertiesModel> GetNumberOfCards(int numberOfCards)
        {
            return _gamePropertiesModelRepository.ListGamePropertiesModel().Where(g => g.NumberOfCards == numberOfCards).ToList();
        }

        public virtual IEnumerable<GamePropertiesModel> GetNumberOfPlayers(int numberOfPlayers)
        {
            return _gamePropertiesModelRepository.ListGamePropertiesModel().Where(g => g.NumberOfPlayers == numberOfPlayers).ToList();
        }

        public virtual IEnumerable<GamePropertiesModel> GetStageId(int stageId)
        {
            return _gamePropertiesModelRepository.ListGamePropertiesModel().Where(g => g.StageId == stageId).ToList();
        }
        //getAll
        public IEnumerable<GamePropertiesModel> GetAll(GamePropertiesModel gamePropertiesModel)
        {
            return _gamePropertiesModelRepository.ListGamePropertiesModel();
        }
        //Delete
        public void DeleteGameProperties(GamePropertiesModel gamePropertiesModelToDelete)
        {
            _gamePropertiesModelRepository.DeleteGamePropertiesModel(gamePropertiesModelToDelete);//stageId
            // return ok?
        }
        //Create
        public GamePropertiesModel CreateGamePropertiesModel(GamePropertiesModel gamePropertiesModelToEdit)
        {
            return _gamePropertiesModelRepository.CreateGamePropertiesModel(gamePropertiesModelToEdit);
        }
        // Save
        public void Save()
        {
            _gamePropertiesModelRepository.Save();
            // return ok?
        }
        //Edit
        public GamePropertiesModel EditGamePropertiesModel(GamePropertiesModel gamePropertiesModelToEdit)//stageId
        {
            return _gamePropertiesModelRepository.EditGamePropertiesModel(gamePropertiesModelToEdit);
        }
    }
}
