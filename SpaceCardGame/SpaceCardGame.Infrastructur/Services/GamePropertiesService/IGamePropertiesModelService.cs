﻿using SpaceCardGame.Domain.Models;

namespace SpaceCardGame.Infrastructur.Services.GamePropertiesService
{
    public interface IGamePropertiesModelService
    {
        //list
        IEnumerable<GamePropertiesModel> GetAll(GamePropertiesModel gamePropertiesModel);
        //read
        IEnumerable<GamePropertiesModel> GetStageId(int stageId);
        IEnumerable<GamePropertiesModel> GetNumberOfCards(int numberOfCards);
        IEnumerable<GamePropertiesModel> GetNumberOfPlayers(int numberOfPlayers);
        IEnumerable<GamePropertiesModel> GetGameTime(int gameTime);
        //delete
        public void DeleteGameProperties(GamePropertiesModel gamePropertiesModelToDelete);//stageId
        //create
        public GamePropertiesModel CreateGamePropertiesModel(GamePropertiesModel gamePropertiesModelToCreate);
        //save
        public void Save();
        //edit
        public GamePropertiesModel EditGamePropertiesModel(GamePropertiesModel gamePropertiesModelToEdit); //stageId
    }
}
