﻿using SpaceCardGame.Domain.Models;

namespace SpaceCardGame.Infrastructur.Services.RoundModelService
{
    public interface IRoundModelService
    {
        IEnumerable<RoundModel> GetAll(RoundModel roundModel);
        IEnumerable<RoundModel> GetRoundId(int roundId);
        IEnumerable<RoundModel> GetOperationType(OperationType operationType);
        public RoundModel CreateRoundModel(RoundModel roundModelToCreate);
        public void DeleteRoundModel(RoundModel roundModelToDelete);
        public void Save();
        public RoundModel EditRoundModel(RoundModel roundModelToEdit);

    }
}

