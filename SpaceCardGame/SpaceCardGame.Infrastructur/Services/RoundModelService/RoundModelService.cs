﻿using SpaceCardGame.Domain.Models;
using SpaceCardGame.Infrastructur.RoundModelCRUD;

namespace SpaceCardGame.Infrastructur.Services.RoundModelService
{

    public class RoundModelService : IRoundModelService
    {
        public IRoundModelRepository? _roundModelRepository { get; }

        public RoundModelService(IRoundModelRepository roundModelRepository)
        {
            _roundModelRepository = roundModelRepository;
        }
        public virtual IEnumerable<RoundModel> GetRoundId(int roundId)
        {
            return _roundModelRepository.ListRoundModel().Where(g => g.RoundId == roundId).ToList();
        }

        public virtual IEnumerable<RoundModel> GetOperationType(OperationType operationType)
        {
            return _roundModelRepository.ListRoundModel().Where(g => g.operationType == operationType).ToList();
        }

        public IEnumerable<RoundModel> GetAll(RoundModel roundModel)
        {
            return _roundModelRepository.ListRoundModel();
        }

        public RoundModel CreateRoundModel(RoundModel roundModel)
        {
            return _roundModelRepository.CreateRoundModel(roundModel);
        }

        public void DeleteRoundModel(RoundModel roundModel)
        {
            _roundModelRepository.DeleteRoundModel(roundModel);

        }
        public void Save()
        {
            _roundModelRepository.Save();

        }
        public RoundModel EditRoundModel(RoundModel roundModel)
        {
            return _roundModelRepository.EditRoundModel(roundModel);
        }

    }
}





