﻿using Moq;
using SpaceCardGame.Domain.Models;
using SpaceCardGame.Infrastructur.GamePropertiesModelCRUD;
using SpaceCardGame.Infrastructur.Services.GamePropertiesService;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;


namespace TestGamePropertiesService.CrudGet
{
    public class CreateGameProperties
    {
        [Fact]
        public void CreateGamePropertiesService_ShouldReturnCorrectResult()
        {
            // Arrange
            var gamePropertyMockModel1 = new GamePropertiesModel() { StageId = 1, GameTime = 1, NumberOfCards = 1, NumberOfPlayers = 1 };
            var gamePropertyMockModel2 = new GamePropertiesModel() { StageId = 1, GameTime = 1, NumberOfCards = 1, NumberOfPlayers = 1 };



            gamePropertiesModelRepositoryMock.Setup(x => x.CreateGamePropertiesModel(gamePropertyMockModel1)).Returns(gamePropertyMockModel1);



            // Act
            var result = serviceUnderTest.CreateGamePropertiesModel(gamePropertyMockModel1);

            // Assert


            Assert.NotNull(result);

        }
        #region CONFIGURATION

        GamePropertiesModelService serviceUnderTest;
        Mock<IGamePropertiesModelRepository> gamePropertiesModelRepositoryMock;

        public CreateGameProperties()
        {
            gamePropertiesModelRepositoryMock = new Mock<IGamePropertiesModelRepository>();
            serviceUnderTest = new GamePropertiesModelService(gamePropertiesModelRepositoryMock.Object);
        }
        #endregion
    }
}