﻿using Moq;
using SpaceCardGame.Domain.Models;
using SpaceCardGame.Infrastructur.GamePropertiesModelCRUD;
using SpaceCardGame.Infrastructur.Services.GamePropertiesService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestGamePropertiesService
{
    public class EditGameProperties
    {
        [Fact]
        public void EditGamePropertiesService_ShouldReturnCorrectResult()
        {
            //Arrange
            var gamePropertyMockModel1 = new GamePropertiesModel() { StageId = 1, GameTime = 1, NumberOfCards = 1, NumberOfPlayers = 1 };
            var gamePropertyMockModel2 = new GamePropertiesModel() { StageId = 1, GameTime = 2, NumberOfCards = 3, NumberOfPlayers = 4 };
            var gamePropertyMockModel3 = new GamePropertiesModel() { StageId = 1, GameTime = 2, NumberOfCards = 3, NumberOfPlayers = 4 };
            var gamePropertyMockModel4 = new GamePropertiesModel() { StageId = 1, GameTime = 2, NumberOfCards = 3, NumberOfPlayers = 4 };

            //// Act
            gamePropertiesModelRepositoryMock.Setup(x => x.EditGamePropertiesModel(gamePropertyMockModel1)).Returns(gamePropertyMockModel2);
            //// Assert
            var result = serviceUnderTest.EditGamePropertiesModel(gamePropertyMockModel1);

            Assert.NotEqual(result, gamePropertyMockModel1);
            Assert.NotEqual(result, gamePropertyMockModel3);
            Assert.NotEqual(result, gamePropertyMockModel4);

            Assert.Equal(result, gamePropertyMockModel2);

        }
        //configure
        #region CONFIGURATION
        GamePropertiesModelService serviceUnderTest;
        Mock<IGamePropertiesModelRepository> gamePropertiesModelRepositoryMock;
        public EditGameProperties()
        {
            gamePropertiesModelRepositoryMock = new Mock<IGamePropertiesModelRepository>();
            serviceUnderTest = new GamePropertiesModelService(gamePropertiesModelRepositoryMock.Object);
        }
        #endregion
    }
}
