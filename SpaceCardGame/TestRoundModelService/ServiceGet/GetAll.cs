﻿using Xunit;
using Moq;
using SpaceCardGame.Infrastructur.Services.RoundModelService;
using SpaceCardGame.Infrastructur.RoundModelCRUD;
using SpaceCardGame.Domain.Models;
using System.Collections.Generic;

namespace RoundModelUnitTests
{
    public class RoundModelGetAll
    {
        [Fact]
        public void GetAllService_ShouldReturnCorrectResult()

        {

            //RoundModel(int ID, int<list> playerlistID, int<list> cardlistID, operationtype) 
            var RoundModelMock1 = new RoundModel(1, OperationType.Battle);
            var RoundModelMock2 = new RoundModel(22, OperationType.CaptureTheFlag);
            List<RoundModel> lista = new List<RoundModel>() { RoundModelMock1, RoundModelMock2 };

            roundModelRepositoryMock.Setup(s => s.ListRoundModel()).Returns(lista);


            new RoundModelGetAll();
            //Act 
            List<RoundModel> result = (List<RoundModel>)serviceUnderTest.GetAll(RoundModelMock1);


            //Assert


            var RoundModelMocktest = new RoundModel(1, OperationType.Battle);


            Assert.Equal(RoundModelMock1, result.FirstOrDefault());
        }
        //configure
        #region CONFIGURATION
        Mock<IRoundModelRepository> roundModelRepositoryMock;
        RoundModelService serviceUnderTest;

        public RoundModelGetAll()
        {
            roundModelRepositoryMock = new Mock<IRoundModelRepository>();
            serviceUnderTest = new RoundModelService(roundModelRepositoryMock.Object);
        }



        #endregion
    }
}
