﻿using Moq;
using SpaceCardGame.Infrastructur.Services.RoundModelService;
using SpaceCardGame.Infrastructur.RoundModelCRUD;
using SpaceCardGame.Domain.Models;

namespace RoundModelUnitTests
{
    public class GetRoundId
    {
        [Fact]
        public void GetRoundIdService_ShouldReturnCorrectResult()
        {
            // Arrange
            {

                //RoundModel(int ID, int<list> playerlistID, int<list> cardlistID, operationtype) 
                var RoundModelMock1 = new RoundModel(1, OperationType.Battle);
                var RoundModelMock2 = new RoundModel(2, OperationType.CaptureTheFlag);
                List<RoundModel> lista = new List<RoundModel>() { RoundModelMock1, RoundModelMock2 };


                roundModelRepositoryMock.Setup(s => s.ListRoundModel()).Returns(lista);

                new GetRoundId();

                // Act
                List<RoundModel> result = (List<RoundModel>)serviceUnderTest.GetRoundId(1);
                List<RoundModel> result2 = (List<RoundModel>)serviceUnderTest.GetRoundId(2);


                // Assert
                int resultInt = result.Find(x => x.RoundId == 1).RoundId;
                int resultInt2 = result2.Find(x => x.RoundId == 2).RoundId;



                RoundModel resultb = result.First();
                Assert.Equal(1, resultInt);
                Assert.Equal(2, resultInt2);


            }

        }

        #region CONFIGURATION
        Mock<IRoundModelRepository> roundModelRepositoryMock;
        RoundModelService serviceUnderTest;

        public GetRoundId()
        {
            roundModelRepositoryMock = new Mock<IRoundModelRepository>();
            serviceUnderTest = new RoundModelService(roundModelRepositoryMock.Object);
        }



        #endregion

    }
}
